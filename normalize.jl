using StatsBase
using TimeSeries

function normalized_data(arr::TimeArray, period_arrays)
    symbols = Symbol[]
    for period in keys(period_arrays)
        for symbol in period_arrays[period]
            push!(symbols, symbol)
        end
    end
    result_array = convert(Matrix{Float32}, values(arr[symbols...])')
    row = 1
    for period in keys(period_arrays)
        period_array_size = length(period_arrays[period])
        end_row = row + period_array_size - 1
        # Normalize neurons with a given period
        if period > 0
            col = size(result_array, 2)
            while col > 0
                # Compute the transformation based off the window
                bottom = max(col - period, 1)
                top = max(col - 1, 1)
                dt = StatsBase.fit(UnitRangeTransform, result_array[row:end_row, bottom:top]; dims=2)
                # Transform the column. If column is low, transform all lower columns
                if col < 4
                    result_array[row:end_row, 1:col] = StatsBase.transform!(
                        dt,
                        result_array[row:end_row, 1:col]
                    )
                    col = 0
                else
                    result_array[row:end_row, col:col] = StatsBase.transform!(
                        dt,
                        result_array[row:end_row, col:col]
                    )
                    col -= 1
                end
            end
        end
        # Move on to the next period
        row = end_row + 1
    end
    result_array
end

function denan!(input_data::Array)
    for i in 1:length(input_data)
        if isnan(input_data[i])
            input_data[i] = 0
        end
    end
end

function deext!(input_data::Array)
    for i in 1:length(input_data)
        if isnan(input_data[i])
            input_data[i] = 0
        end
        if input_data[i] > 2
            input_data[i] = 2
        end
        if input_data[i] < -2
            input_data[i] = -2
        end
    end
end

function normdiff(arr::TimeArray, period::Int64)
    normalized = normalized_data(
        arr,
        Dict(period => colnames(arr))
    );
    deext!(normalized)
    for ix in reverse(2:(size(normalized, 2)))
        normalized[ix] -= normalized[ix - 1]
    end
    return normalized
end