# Precompile the libraries used here into a sysimage, along with some plotting tools, for fast debugging
# Use the sanity test for "profile guided compilation".
# Mind you this is a sin and there should be a global cache...

@info "Initializing"
using ArgParse

s = ArgParseSettings()
@add_arg_table! s begin
    "--replace-default"
        help = "Replace the default sysimage with the generated one. Use PackageCompiler.jl's restore_default_sysimage() to undo this!"
        action = :store_true
    "--sysimage-path"
        help = "The path for this sysimage. If empty, do not export a sysimage"
        default = "sys_stocks.so"
end
parsed_args = parse_args(ARGS, s)

@info "Loading package compiler"
@time using PackageCompiler
replace_default = parsed_args["replace-default"]
sysimage_path = parsed_args["sysimage-path"]
if sysimage_path == "" || replace_default
    sysimage_path = nothing
end
@info "Creating sysimage @ $sysimage_path, with replace-default=$(replace_default)"
packages = [
    :Knet,
    :CSV,
    :DataFrames,
    :TimeSeries,
    :Plots,
    :Indicators
]
@time create_sysimage(
    packages; 
    sysimage_path = sysimage_path, 
    precompile_execution_file = "sanity-test.jl", 
    replace_default =replace_default
    )
@info "Made image @ $sysimage_path"