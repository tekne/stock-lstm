using Indicators
using TimeSeries

function Indicators.sma(ts::TimeArray; n::Int64=10)::TimeArray
    smas = [
        TimeArray(
            timestamp(ts[name]),
            sma(values(ts[name]); n=n),
            [Symbol(String(name) * "_sma")]
        ) for name in colnames(ts)
    ]
    if length(smas) == 1
        smas[1]
    else
        merge(smas...)
    end
end

function Indicators.sma(ts::TimeArray, ns::Array{Int64})::TimeArray
    smas = [
        rename!(
            Symbol ∘ (s -> s * "_$n") ∘ String, 
            sma(ts; n=n)
        ) for n in ns
    ]
    if length(smas) > 1
        merge(smas...)
    else
        smas[1]
    end
end

function Indicators.ema(ts::TimeArray; n::Int64=10, alpha=2.0/(n + 1.0))::TimeArray
    emas = [
        TimeArray(
            timestamp(ts[name]),
            ema(values(ts[name]); n=n, alpha=alpha),
            [Symbol(String(name) * "_ema")]
        ) for name in colnames(ts)
    ]
    if length(emas) == 1
        emas[1]
    else
        merge(emas...)
    end
end

function Indicators.ema(ts::TimeArray, ns::Array{Int64}; alpha=n -> 2.0/(n + 1.0))::TimeArray
    emas = [
        rename!(
            Symbol ∘ (s -> s * "_$n") ∘ String, 
            ema(ts; n=n, alpha=alpha(n))
        ) for n in ns
    ]
    if length(emas) > 1
        merge(emas...)
    else
        emas[1]
    end
end

function Indicators.dema(ts::TimeArray; n::Int64=10, alpha=2.0/(n+1), wilder::Bool=false)::TimeArray
    demas = [
        TimeArray(
            timestamp(ts[name]),
            dema(values(ts[name]); n=n, alpha=alpha),
            [Symbol(String(name) * "_dema")]
        ) for name in colnames(ts)
    ]
    if length(demas) == 1
        demas[1]
    else
        merge(demas...)
    end
end

function Indicators.tema(ts::TimeArray; n::Int64=10, alpha=2.0/(n+1), wilder::Bool=false)::TimeArray
    temas = [
        TimeArray(
            timestamp(ts[name]),
            tema(values(ts[name]); n=n, alpha=alpha),
            [Symbol(String(name) * "_tema")]
        ) for name in colnames(ts)
    ]
    if length(temas) == 1
        temas[1]
    else
        merge(temas...)
    end
end

function Indicators.macd(ts::TimeArray; nfast::Int64=12, nslow::Int64=26, nsig::Int64=9)::TimeArray
    TimeArray(
        timestamp(ts[:close]),
        macd(values(ts[:close]); nfast=nfast, nslow=nslow, nsig=nsig),
        [
            Symbol("macd_$(nfast)_$(nslow)_$(nsig)"), 
            Symbol("macd_signal_$(nfast)_$(nslow)_$(nsig)"), 
            Symbol("macd_histogram_$(nfast)_$(nslow)_$(nsig)")
        ]
    )
end

function Indicators.stoch(ts::TimeArray; nK::Int64=14, nD::Int64=3, kind::Symbol=:fast, ma::Function=sma, args...)::TimeArray
    TimeArray(
        timestamp(ts),
        stoch(values(ts[:high, :low, :close]); nK=nK, nD=nD, kind=kind, ma=ma, args...),
        [Symbol("stoch_$(nK)_$(nD)_$(String(kind))"), Symbol("stoch_signal_$(nK)_$(nD)_$(String(kind))")]
    )
end 

function Indicators.bbands(ts::TimeArray; n::Int64=10, sigma=2.0)::TimeArray
    bbands = [
        TimeArray(
            timestamp(ts),
            Indicators.bbands(values(ts[name]); n=n, sigma=sigma),
            [
                Symbol(String(name) * "_bb_low_$(n)_$sigma"),
                Symbol(String(name) * "_bb_med_$(n)_$sigma"),
                Symbol(String(name) * "_bb_hgh_$(n)_$sigma")
            ]
        )
        for name in colnames(ts)
    ]
    if length(bbands) == 1
        bbands[1]
    else
        merge(bbands...)
    end
end

function Indicators.bbands(ts::TimeArray, ns::Array{Int64}; sigma=2.0)::TimeArray
    bbands = [
        rename!(
            name -> Symbol(String(name) * "_$n"),
            Indicators.bbands(ts; n=n, sigma=sigma)
        )
        for n in ns
    ]
    if length(bbands) == 1
        bbands[1]
    else
        merge(bbands...)
    end
end

function Indicators.rsi(ts::TimeArray; n::Int64=14, ma::Function=ema, args...)::TimeArray
    TimeArray(
        timestamp(ts),
        Indicators.rsi(values(ts[:close])),
        [Symbol("rsi_$n")]
    )
end

function Indicators.cci(ts::TimeArray; n::Int64=20, c=0.015, ma::Function=sma)::TimeArray
    TimeArray(
        timestamp(ts),
        Indicators.cci(values(ts[:high, :low, :close]); n=n, c=c, ma=ma),
        [Symbol("cci_$n")]
    )
end

function Indicators.mlr_bands(ts::TimeArray; n::Int64=10, se=2.0)::TimeArray
    mlr_bands = [
        TimeArray(
            timestamp(ts),
            Indicators.mlr_bands(values(ts[name]); n=n, se=se),
            [
                Symbol(String(name) * "_mlrlb_$(n)_$se"),
                Symbol(String(name) * "_mlrre_$(n)_$se"),
                Symbol(String(name) * "_mlrub_$(n)_$se")
            ]
        )
        for name in colnames(ts)
    ]
    if length(mlr_bands) == 1
        mlr_bands[1]
    else
        merge(mlr_bands...)
    end
end