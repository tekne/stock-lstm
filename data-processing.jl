# The data processing utilities for the 1 minute Polygon stock data we wish to use to replicate the paper's experiments

using TimeSeries
using Indicators
using ProgressMeter
using Knet: KnetArray, minibatch, Data
include("adapted-indicators.jl")

function compute_diffs(data::TimeArray)::TimeArray
    intra_diffs = rename!(
        data[:open, :high, :open, :high] .- data[:close, :open, :low, :low],
        [
            :dp, # Price differential
            :up, # Maximum updward motion
            :down, # Maximum downward motion
            :spread  # Spread
        ]
    )
    average_trade_size = rename!(data[:volume] ./ data[:no_trades], :ats)
    price_factor = rename!(intra_diffs[:dp] ./ data[:open], :price_fact)
    spread_factor = rename!(intra_diffs[:spread] ./ data[:open], :spread_fact)
    spread_diff = rename!(diff(intra_diffs[:spread]), :dspread)
    volume_diff = rename!(diff(data[:volume]), :dv)
    vwap_diff = rename!(diff(data[:vwap]), :dvwap)  
    merge(
        average_trade_size,
        intra_diffs,
        price_factor,
        spread_factor,
        spread_diff,
        volume_diff,
        vwap_diff                           
    )
end

function probabilities(data::TimeArray)::TimeArray
    error("Unimplemented")
end

function technical_array(data::TimeArray)::TimeArray
    merge(
        sma(data[:close]),
        ema(data[:close]),
        dema(data[:close]),
        tema(data[:close]),
        macd(data),
        stoch(data),
        bbands(data[:close]),
        rsi(data),
        cci(data),
        mlr_bands(data[:close])
    )
end

function should_buy(data::TimeArray, t = Float64)::TimeArray
    rename!(t.(data[:open] .< data[:close]), :buy)
end

function make_clock(dates::AbstractArray{DateTime}, periods::AbstractArray{Tuple{String,Int64}})::TimeArray
    clocks = zeros(length(dates), 2 * length(periods))
    for (i, date) in enumerate(dates)
        for (j, (_, period)) in enumerate(periods)
            clocks[i, 2 * j - 1] = sin(2 * π * Dates.value(date) / period)
            clocks[i, 2 * j] = cos(2 * π * Dates.value(date) / period)
        end
    end
    names = Symbol[]
    for (name, _) in  periods
        push!(names, Symbol("$(name)_sin"))
        push!(names, Symbol("$(name)_cos"))
    end
    TimeArray(
        dates,
        clocks,
        names
    )
end

const MILLISECONDS_IN_YEAR = 365 * 24 * 60 * 60 * 1000;
const MILLISECONDS_IN_MONTH = 30 * 24 * 60 * 60 * 1000;
const MILLISECONDS_IN_DAY = 24 * 60 * 60 * 1000;
const MILLISECONDS_IN_HOUR = 60 * 60 * 1000;
const DEFAULT_CLOCKS = [
    ("year", MILLISECONDS_IN_YEAR),
    ("month", MILLISECONDS_IN_MONTH),
    ("day", MILLISECONDS_IN_DAY),
    ("hour", MILLISECONDS_IN_HOUR)
]

function make_input_data(data::TimeArray; clocks = DEFAULT_CLOCKS)::TimeArray
    diffs = compute_diffs(data)
    buy = should_buy(data)
    # probabilities = probabilities(should_buy)
    technicals = technical_array(data)
    to_merge = [
        data,
        diffs,
        buy,
        # probabilities,
        technicals,
    ]
    if length(clocks) != 0
        push!(to_merge, make_clock(timestamp(data), clocks))
    end
    merge(to_merge...)
end

function make_data(data_dict::AbstractDict; logging = false, clocks = DEFAULT_CLOCKS)::TimeArray
    input_data = 
    if logging
        @info "Constructing input data..."
        @showprogress [
            rename!(Symbol ∘ (s->name * "_" * s) ∘ String, make_input_data(data; clocks = [])) for (name, data) in data_dict
        ]
    else
        [
            rename!(Symbol ∘ (s->name * "_" * s) ∘ String, make_input_data(data; clocks = [])) for (name, data) in data_dict
        ]
    end
    if length(clocks) != 0
        push!(input_data, make_clock(timestamp(first(values(data_dict))), clocks))
    end
    if length(input_data) == 1
        return input_data[1]
    else
        return merge(input_data...)
    end
end

function fake_data(n, base_date = DateTime(2020); base_price = 40, flux = 10, base_volume = 1000, min_trade_size = 50, base_trade_size = 200, win_lose = 0.5, smooth = 100)
    lmmh = rand(3 * smooth + n, 4)
    lmmh[(3 * smooth + 1):end, 1] = dema(lmmh[:, 1]; n = smooth)[(3 * smooth + 1):end]
    lmmh[(3 * smooth + 1):end, 2] = dema(lmmh[:, 2]; n = smooth)[(3 * smooth + 1):end]
    lmmh[(3 * smooth + 1):end, 3] = dema(lmmh[:, 3]; n = smooth)[(3 * smooth + 1):end]
    lmmh[(3 * smooth + 1):end, 4] = dema(lmmh[:, 4]; n = smooth)[(3 * smooth + 1):end]
    lmmh = lmmh[(3 * smooth + 1):end, :]
    for i in 1:n
        lmmh[i, :] = sort!(lmmh[i, :])
    end
    ohlcvwn = zeros(n, 7)
    for i in 1:n
        win = rand() < win_lose
        ohlcvwn[i, 1] = base_price + flux * if win lmmh[i, 2] else lmmh[i, 3] end
        ohlcvwn[i, 2] = base_price + flux * lmmh[i, 4] 
        ohlcvwn[i, 3] = base_price + flux * lmmh[i, 1]
        ohlcvwn[i, 4] = base_price + flux * if win lmmh[i, 3] else lmmh[i, 2] end
        ohlcvwn[i, 5] = rand() * base_volume
        ohlcvwn[i, 6] = ohlcvwn[i, 2] + flux * (ohlcvwn[i, 5] - base_volume / 2) / (2 * base_volume)
        ohlcvwn[i, 7] = 1 + round(ohlcvwn[i, 5] / (min_trade_size + base_trade_size * rand()))
    end
    dates = [base_date + Minute(i) for i in 1:n]
    TimeArray(dates, ohlcvwn, [:open, :high, :low, :close, :volume, :vwap, :no_trades])
end

function mb_con(inputs, outputs; seqlength::Int64 = SEQLENGTH, T = Float32, gpu = true)::Data
    inputs = convert(Matrix{T}, inputs')
    outputs = convert(Matrix{T}, outputs')
    if gpu
        inputs = KnetArray(inputs)
        outputs = KnetArray(outputs)
    end
    minibatch(
        inputs,
        outputs,
        seqlength
    )
end

function mb(inputs::TimeArray, outputs::TimeArray; o...)::Data
    ml = min(length(inputs), length(outputs))
    inputs = inputs[1:ml]
    outputs = outputs[1:ml]
    mb_con(values(inputs), values(outputs); o...)
end


function mb(inputs::Array, outputs::Array; o...)::Data
    ml = min(size(inputs, 1), size(outputs, 1))
    inputs = inputs[1:ml, :]
    outputs = outputs[1:ml, :]
    mb_con(inputs, outputs)
end

function mb(data::Tuple; o...)::Data
    mb(data[1], data[2]; o...)
end

function load_stock_data(
    keys,
    key_to_path::Function; 
    format::String = "yyyy-mm-dd HH:MM:SS", 
    logging::Bool = false
    )::TimeArray
    series = if logging
        @info "Reading $(length(keys)) files"
        series = Dict(@showprogress [
            (key, rename!(
                    readtimearray(key_to_path(key); format = format),
                    :o => :open,
                    :h => :high,
                    :l => :low,
                    :c => :close,
                    :v => :volume,
                    :vw => :vwap,
                    :n => :no_trades
                ))
            for key in keys
        ])
    else
        series = Dict([
            (key, rename!(
                    readtimearray(key_to_path(key); format = format),
                    :o => :open,
                    :h => :high,
                    :l => :low,
                    :c => :close,
                    :v => :volume,
                    :vw => :vwap,
                    :n => :no_trades
                ), key)
            for key in keys
        ])
    end
    make_data(series; logging = logging)
end

function truncation_hack(trunc::TimeArray, times::TimeArray)
    getindex(trunc, timestamp(times))
end