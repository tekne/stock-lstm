using Knet
using Knet: Knet, AutoGrad, Data, param, param0, mat, RNN, dropout, value, adam, minibatch, progress!, converge
using TimeSeries
using ProgressMeter

struct Linear; w; b; end

Linear(input::Int, output::Int)=Linear(param(output,input), param0(output))

(l::Linear)(x) = l.w * mat(x,dims=1) .+ l.b  # (H,B,T)->(H,B*T)->(V,B*T)

struct Chain
    layers
    Chain(layers...) = new(layers)
end

using Statistics: mean

(c::Chain)(x; o...) = (for l in c.layers; x = l(x; o...); end; x)
function (c::Chain)(x,y; o...)
    yhat = c(x; o...)
    #TODO: nll...
    sum(abs2, y - yhat) / length(y)
end
(c::Chain)(d::Data; o...) = mean(c(x,y; o...) for (x,y) in d)

RNNTYPE = :lstm;
BATCHSIZE = 64;
SEQLENGTH = 64;
HIDDENSIZE = 512;
NUMLAYERS = 2;

StockLSTM(inputs::Int, outputs::Int; hidden=HIDDENSIZE, numLayers=NUMLAYERS, rnnType=RNNTYPE, o...) =
    Chain(RNN(inputs, hidden; numLayers=NUMLAYERS, rnnType=rnnType, o...), Linear(hidden, outputs)) 

function train(model, train_data)
    model.layers[1].h = nothing
    model.layers[1].c = nothing
    a = adam(model, train_data)
    losses = Float32[]
    for loss in a
        push!(losses, loss)
    end
    return losses
end

function test(model, test_data)
    losses = Float32[]
    n = length(test_data)
    for (input_data, output_data) in test_data
        loss = model(input_data, output_data)
        push!(losses, loss)
    end
    return losses
end