@info "Starting up"

@info "Loading data processing"
@time include("data-processing.jl")
@info "Loading model maker"
@time include("make-model.jl")
@info "Loading normalization utilities"
@time include("normalize.jl")

const FAKE_DATA_SIZE = 1000000

@info "Generating $FAKE_DATA_SIZE points of fake data (1/3)"
@time fake = fake_data(FAKE_DATA_SIZE)
@info "Generating $FAKE_DATA_SIZE points of fake data (2/3)"
@time phony = fake_data(FAKE_DATA_SIZE)
@info "Generating $FAKE_DATA_SIZE points of fake data (3/3)"
@time falsehood = fake_data(FAKE_DATA_SIZE)

@info "Making fake I/O data"
@time data = make_data(
    Dict(
        "FAKE" => fake,
        "PHNY" => phony,
        "FLSE" => falsehood
    )
)

@info "Normalizing fake input output_data"
@time nrm = normalized_data(
    data,
    Dict(
        64 => colnames(input_data)
    )
)

@info "Importing plots"
@time using Plots

@info "Plotting the input data"
@time Plots.plot(data)

@info "Plotting normalized input data"
@time Plots.plot(timestamp(data), nrm[:, 1])

@info "Sanity test complete!"